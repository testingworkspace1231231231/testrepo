var net = require('net'),
  JsonSocket = require('json-socket');
const { debugPort } = require('process');
var socket = new JsonSocket(new net.Socket()); //Decorate a standard net.Socket with JsonSocket
var port = 9838; //The same port that the server is listening on
var host = '127.0.0.1';
// var port = 3100;
// var host = '78.154.174.18';
//
socket.connect(port, host);
socket.on('connect', function () {
  //Don't send until we're connected
  socket.sendMessage({ command: 'start' });
  socket.on('message', function (message) {
    if (message.command == 'auth_success') {
      authSuccess(message);
    } else if (message.command == 'auth_failed') {
      document.getElementById('alert').innerHTML = 'Введено не коректні данні';
    } else if (message.command == 'registr_success') {
      registrSuccess();
    } else if (message.command == 'registr_failed') {
      document.getElementById('alert_registration').innerHTML += 'Даний логін занятий';
    } else if (message.command == 'load_train_success') {
      drawTrains(message);
    } else if (message.command == 'load_data_success') {
      drawData(message);
    } else if (message.command == 'purchase_success') {
      document.getElementById('purchase_alert').innerHTML = 'Квиток придбано';
    } else if (message.command == 'reserve_success') {
      document.getElementById('purchase_alert').innerHTML = 'Квиток заброньовано';
    } else if (message.command == 'purchase_failed') {
      document.getElementById('purchase_alert').innerHTML = 'Щось сталось не так, виберіть інше місце';
    } else if (message.command == 'ticket_list_success') {
      drawTickets(message);
    } else if (message.command == 'reserve_list_success') {
      drawTickets(message);
    }
  });
});

// Функції
//

function registrSuccess() {
  localStorage.setItem('login', document.getElementById('login').value);
  localStorage.setItem('firstName', document.getElementById('first_name').value);
  localStorage.setItem('lastName', document.getElementById('last_name').value);
  localStorage.setItem('email', document.getElementById('email').value);
  window.location.href = '../main_page/main_page.html';
}
function authSuccess(message) {
  localStorage.setItem('login', message.user.login);
  localStorage.setItem('firstName', message.user.FirstName);
  localStorage.setItem('lastName', message.user.LastName);
  localStorage.setItem('email', message.user.email);
  document.getElementById('alert').innerHTML = 'Вхід....';
  window.location.href = './main_page/main_page.html';
}
function authentication() {
  let login = document.getElementById('login').value;
  let password = document.getElementById('password').value;
  console.log(login + ' ' + password + '-test');
  socket.sendMessage({
    command: 'auth',
    login: login,
    password: password,
  });
}

function registration() {
  document.getElementById('alert_registration').innerHTML = '';
  let login = document.getElementById('login').value;
  let password = document.getElementById('password').value;
  let rePassword = document.getElementById('re_password').value;
  let firstName = document.getElementById('first_name').value;
  let lastName = document.getElementById('last_name').value;
  let email = document.getElementById('email').value;
  if (
    validateLoginRegistration(login) == 1 &&
    validateEmailRegistration(email) == 1 &&
    validateStrRegistration(firstName) == 1 &&
    validateStrRegistration(lastName) == 1 &&
    validatePasswordRegistration(password, rePassword) == 1
  ) {
    socket.sendMessage({
      command: 'registr',
      login: login,
      password: password,
      firstName: firstName,
      lastName: lastName,
      email: email,
    });
  } else {
    console.log('ne PODXODIT');
  }
}

function purchase() {
  document.getElementById('purchase_alert').innerHTML = ' ';
  let place = +document.getElementById('place').value;
  let car = +document.getElementById('car').value;
  if (place == 0 || car == 0) {
    console.log(document.getElementById('train').value);
    document.getElementById('purchase_alert').innerHTML = 'Ведіть будь ласка місце і вагон';
  } else if (place > 52 || car > 20) {
    document.getElementById('purchase_alert').innerHTML = 'У поїзді 20 вагонів і 52 місця.';
  } else if (document.getElementById('data').value == '') {
    document.getElementById('purchase_alert').innerHTML = 'Виберіть час';
  } else if (Number.isInteger(place) == true && Number.isInteger(car) == true && place < 53 && car < 21) {
    socket.sendMessage({
      command: 'ticket_purchase',
      login: localStorage.getItem('login'),
      route: document.getElementById('trains').value,
      date_time: document.getElementById('data').value,
      place: place,
      car: car,
    });
  } else {
    document.getElementById('purchase_alert').innerHTML = 'Введіть будь ласка число';
  }
}

function reserve() {
  document.getElementById('purchase_alert').innerHTML = ' ';
  let place = +document.getElementById('place').value;
  let car = +document.getElementById('car').value;
  if (place == 0 || car == 0) {
    console.log(document.getElementById('train').value);
    document.getElementById('purchase_alert').innerHTML = 'Ведіть будь ласка місце і вагон';
  } else if (place > 52 || car > 20) {
    document.getElementById('purchase_alert').innerHTML = 'У поїзді 20 вагонів і 52 місця.';
  } else if (document.getElementById('data').value == '') {
    document.getElementById('purchase_alert').innerHTML = 'Виберіть час';
  } else if (Number.isInteger(place) == true && Number.isInteger(car) == true && place < 53 && car < 21) {
    socket.sendMessage({
      command: 'ticket_reserve',
      login: localStorage.getItem('login'),
      route: document.getElementById('trains').value,
      date_time: document.getElementById('data').value,
      place: place,
      car: car,
    });
  } else {
    document.getElementById('purchase_alert').innerHTML = 'Введіть будь ласка число';
  }
}

function ticketList() {
  socket.sendMessage({
    command: 'ticket_list',
    login: localStorage.getItem('login'),
  });
}
function reserveList() {
  socket.sendMessage({
    command: 'reserve_list',
    login: localStorage.getItem('login'),
  });
}

function drawTickets(message) {
  document.getElementById('ticket_list').innerHTML = '';
  console.log(message.data);
  if (message.data[0].car != null) {
    for (let key in message.data) {
      document.getElementById('ticket_list').innerHTML += `<div>Маршрут: ${
        message.data[key].route
      }, Відправлення:${transformDate(message.data[key].date_time)}, Вагон:${message.data[key].car} Місце: ${
        message.data[key].place
      } </div>`;
    }
  } else {
    for (let key in message.data) {
      document.getElementById('ticket_list').innerHTML += `<div>Маршрут: ${
        message.data[key].route
      }, Відправлення:${transformDate(message.data[key].date_time)}, Вагон:${message.data[key].reserved_car} Місце: ${
        message.data[key].reserved_place
      } </div>`;
    }
  }
}

function validateLogin(login) {
  var re = /^[\w\dА-я]{4,}$/;
  return re.test(login);
}

function validateStr(str) {
  var re = /^[a-zA-Z]{2,}$/;
  return re.test(str);
}

function validateEmail(email) {
  var re = /^[a-zA-Z0-9!#.$%^&*]{2,30}?[@]{1,1}?[a-zA-Z0-9!#.$%^&*]{2,}$/;
  return re.test(email);
}

function validatePassword(password, rePassword) {
  var re = /^[a-zA-Z0-9!@#$%^&*]{4,16}$/;
  return re.test(password);
}

function backToMain() {
  window.location.href = '../main_page/main_page.html';
}

function validateLoginRegistration(login) {
  if (login == '') {
    document.getElementById('alert_registration').innerHTML += 'Необхідно заповнити всі поля ';
  } else if (validateLogin(login) == 0) {
    document.getElementById('alert_registration').innerHTML +=
      'Логін може використовувати тільки букви та цифри. Довжина логіна не має бути меншою ніж 4 символи' + ' ';
  } else if (validateLogin(login) == 1) {
    console.log('login-1');
    return 1;
  }
}

function validatePasswordRegistration(password, rePassword) {
  if (password == rePassword) {
    if (password == '') {
      document.getElementById('alert_registration').innerHTML += 'Поле пароль не може бути пустим' + ' ';
    } else if (validatePassword(password) == 0) {
      document.getElementById('alert_registration').innerHTML +=
        'Пароль може використовувати тільки букви та цифри. Довжина пароля не має бути меншою ніж 4 символи' + ' ';
    } else if (validatePassword(password) == 1) {
      console.log('parol-1');
      return 1;
    }
  } else {
    document.getElementById('alert_registration').innerHTML += 'Паролі не співпадають' + ' ';
  }
}

function validateEmailRegistration(email) {
  if (email == '') {
    document.getElementById('alert_registration').innerHTML += 'Заповніть всі поля';
  } else if (validateEmail(email) == 0) {
    document.getElementById('alert_registration').innerHTML += 'Даний email не є коректним';
  } else if (validateEmail(email) == 1) {
    console.log('email-1');
    return 1;
  }
}

function validateStrRegistration(str) {
  if (str == '') {
    document.getElementById('alert_registration').innerHTML += 'Заповніть всі поля' + ' ';
  } else if (validateStr(str) == 0) {
    document.getElementById('alert_registration').innerHTML += 'Поле може використовувати тільки букви' + ' ';
  } else if (validateStr(str) == 1) {
    console.log('str-1');
    return 1;
  }
}

function transformDate(date) {
  date = date.replace('T', ' ');
  date = date.replace('Z', ' ');
  return date;
}
function loadTrains() {
  socket.sendMessage({
    command: 'load-trains',
  });
}
function loadData(value) {
  socket.sendMessage({
    command: 'load_data',
    route: value,
  });
}

function drawTrains(message) {
  document.getElementById('trains_out').innerHTML += '<select class ="btn" id="trains" onchange="loadData(value)">';
  document.getElementById('trains').innerHTML += '<option hidden disabled selected value> Виберіть маршрут </option>';
  for (let key in message.trains) {
    document.getElementById(
      'trains'
    ).innerHTML += `<option value="${message.trains[key].route}">${message.trains[key].route}</option>`;
  }
  document.getElementById('trains_out').innerHTML += '</select>';
}

function drawData(message) {
  document.getElementById('data_out').innerHTML = '';
  console.log(message);
  document.getElementById('data_out').innerHTML += '<select class ="btn" id="data" >';
  document.getElementById('data').innerHTML +=
    '<option hidden disabled selected value id="void"> Виберіть час </option>';
  for (let key in message.data) {
    document.getElementById('data').innerHTML += `<option>${transformDate(message.data[key].date_time)}</option>`;
  }
  document.getElementById('data_out').innerHTML += '</select>';
}
